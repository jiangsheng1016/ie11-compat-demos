var statusDiv = document.getElementById('status');
document.getElementById('upload').onclick = function () {
    var filePath = document.getElementById('file-path').value;
    var stream = new ActiveXObject('ADODB.Stream');
    stream.type = 1;
    stream.open();

    var xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');

    stream.loadFromFile(filePath);
    stream.position = 0;

    xmlHttp.onreadystatechange = function (event) {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            statusDiv.innerHTML = xmlHttp.responseText;
        }
    };
    var url = '/upload?filename=' + escape(getFileNameWithExt(filePath));
    xmlHttp.open("POST", url, true);
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlHttp.send(stream);
    statusDiv.innerHTML = '上传中...';
}
function getFileNameWithExt(fileName) {
    var nFileNameStart = fileName.lastIndexOf("\\");
    return fileName.substring(nFileNameStart + 1, fileName.length);
}