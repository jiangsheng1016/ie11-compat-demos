var TypeClass = function () {
    this.agree = 1;
    this.disagree = 1;
    this.onhold = 1;
    return this;
}

TypeClass.prototype.toString = function () {
    return 'This is type class';
};

var options = {
    agree: '同意',
    disagree: '不同意',
    onhold: '待定'
};

var typeInstance = new TypeClass();

var select = document.getElementById('select');

for (var name in typeInstance) {
    if (options[name]) {
        var opt = document.createElement('option');
        select.options.add(opt);
        opt.innerText = options[name];
        opt.value = name;
    }
}