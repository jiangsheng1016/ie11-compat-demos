var express = require('express');
var app = express();

app.use('/static', express.static('static'));
app.use('/upload', express.static('upload'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/ua-1', function (req, res) {
    var userAgent = req.headers['user-agent'];
    if (userAgent.indexOf('MSIE') > -1) {
        res.send('<h1>欢迎使用本系统</h1>');
    } else {
        res.send('<h1 style="color:red">请使用IE浏览器</h1>');
    }
});

app.get('/upload', function (req, res) {
    res.sendFile(__dirname + '/upload/default.html');
});

app.post('/upload', function (req, res) {
    setTimeout(function () {
        res.send('上传成功：' + req.query.filename);
    }, 1000);
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});